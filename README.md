# Test API Perpustakaan

Contoh API perpustakaan menggunakan java 8 dan open-api untuk test PT Rumah Kreasi Aplikasi.

## Maven dependencies

```
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-data-jpa</artifactId>
</dependency>
<dependency>
	<groupId>org.springframework.boot</groupId>
	<artifactId>spring-boot-starter-web</artifactId>
</dependency>
<dependency>
	<groupId>org.springdoc</groupId>
	<artifactId>springdoc-openapi-ui</artifactId>
	<version>1.4.4</version>
</dependency>
<dependency>
	<groupId>mysql</groupId>
	<artifactId>mysql-connector-java</artifactId>
	<scope>runtime</scope>
</dependency>

```
## Install

MySql install [windows](https://dev.mysql.com/doc/mysql-installation-excerpt/8.0/en/windows-installation.html) atau [linux](https://dev.mysql.com/doc/mysql-installation-excerpt/8.0/en/linux-installation.html)

Java install [klik](https://www.digitalocean.com/community/tutorials/how-to-install-java-with-apt-on-ubuntu-18-04)

Maven install [klik](https://maven.apache.org/install.html)

Git install [klik](https://github.com/git-guides/install-git)

```bash
git clone https://gitlab.com/iikmaulana369/testperpustakaan.git
cd testperpustakaan/
mvn clean install
```
## Config Database

Create database di MySql dengan nama dabase "test_perpustakaan"

Edit config ada di file application.yml di folder /testperpustakaan/src/main/resources


## Run
```bash
cd target/
java -jar com.test.perpustakaan-0.0.1-SNAPSHOT-spring-boot.jar
```

- akses "http://localhost:8080/swagger-ui.html"