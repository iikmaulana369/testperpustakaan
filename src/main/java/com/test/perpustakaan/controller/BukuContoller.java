package com.test.perpustakaan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.perpustakaan.entity.Buku;
import com.test.perpustakaan.services.BukuService;

@RestController
@RequestMapping(value = "/api")
public class BukuContoller {

	@Autowired
	private BukuService bukuService;

	@CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = { "accept", "authorization", "x-requested-with", "content-type" })
	@PostMapping(value = "/buku")
	public Map<String, Object> saveBuku(@RequestBody Buku buku) {
		Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		bukuService.save(buku);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat menyimpan data buku");
	        return map;
		}
        
		map.put("success", Boolean.TRUE);
		map.put("info", "data buku baru berhasil di simpan ");
		return map;
	}
	
	@CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = { "accept", "authorization", "x-requested-with", "content-type" })
	@PutMapping(value = "/buku")
	public Map<String, Object> updateBuku(@RequestBody Buku buku) {
		Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		bukuService.update(buku);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat update data buku");
	        return map;
		}
        
		map.put("success", Boolean.TRUE);
		map.put("info", "data buku baru berhasil di update ");
		return map;
	}
	
	@CrossOrigin(origins = "*", methods = RequestMethod.GET, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @GetMapping(value = "/getbuku")
    public List<Buku> getBuku() {
        return bukuService.getBuku();
    }


}
