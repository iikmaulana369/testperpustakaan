package com.test.perpustakaan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.perpustakaan.entity.Buku;
import com.test.perpustakaan.entity.Mahasiswa;
import com.test.perpustakaan.entity.Peminjaman;
import com.test.perpustakaan.services.BukuService;
import com.test.perpustakaan.services.MahasiswaService;
import com.test.perpustakaan.services.PeminjamanService;

@RestController
@RequestMapping(value = "/api")
public class PeminjamanController {
	
	public Mahasiswa mahasiswa = new Mahasiswa();
	public Buku buku = new Buku();
	
	@Autowired
    private PeminjamanService peminjamanService;
	
	@Autowired
    private BukuService bukuService;
	
	@Autowired
    private MahasiswaService mahasiswaService;
	
	@Autowired
	private KafkaTemplate<Object, Object> kafkaTemplate;

    @CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @PostMapping(value = "/peminjaman/{nim}/{idBuku}")
    public Map<String, Object> savePeminjaman(@PathVariable (value = "nim") String nim, @PathVariable (value = "idBuku") String idBuku, @RequestBody Peminjaman peminjaman) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		mahasiswa = mahasiswaService.getMahasiswa(nim);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat mengambil data mahasiswa");
	        return map;
		}
    	
    	try {
    		buku = bukuService.getBuku(idBuku);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat mengambil data buku");
	        return map;
		}
    	
    	if (mahasiswa == null) {
    		map.put("success", Boolean.FALSE);
	        map.put("info", "NIM Mahasiswa tidak terdaftar");
	        return map;
    	}
    	
    	if (buku == null) {
    		map.put("success", Boolean.FALSE);
	        map.put("info", "ID Buku tidak terdaftar");
	        return map;
    	}
    	
    	if (buku.getJumlahBuku() < 1) {
    		map.put("success", Boolean.FALSE);
	        map.put("info", "Buku tidak tersedia");
	        return map;
    	}
    	
    	peminjaman.setMahasiswa(mahasiswa);
    	peminjaman.setBuku(buku);
        
        try {
        	peminjamanService.save(peminjaman);
        	
        	this.kafkaTemplate.send("pengembalian", peminjaman);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat menyimpan data peminjaman");
	        return map;
		}
        
        buku.setJumlahBuku(buku.getJumlahBuku() - peminjaman.getJumlah());

        try {
        	bukuService.update(buku);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat update data buku");
	        return map;
		}
        
        map.put("success", Boolean.TRUE);
        map.put("info", "data peminjaman berhasil di simpan ");
        return map;
    }
    
    @CrossOrigin(origins = "*", methods = RequestMethod.GET, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @GetMapping(value = "/getPeminjaman")
    public List<Peminjaman> getPeminjaman() {
        return peminjamanService.getPeminjaman();
    }

}
