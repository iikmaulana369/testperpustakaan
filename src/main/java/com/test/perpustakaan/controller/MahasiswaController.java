package com.test.perpustakaan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.perpustakaan.entity.Mahasiswa;
import com.test.perpustakaan.services.MahasiswaService;

@RestController
@RequestMapping(value = "/api")
public class MahasiswaController {
	
	@Autowired
    private MahasiswaService mahasiswaService;

    @CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @PostMapping(value = "/mahasiswa")
    public Map<String, Object> saveMahasiswa(@RequestBody Mahasiswa mahasiswa) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		mahasiswaService.save(mahasiswa);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat menyimpan data mahasiswa");
	        return map;
		}
        
        map.put("success", Boolean.TRUE);
        map.put("info", "data mahasiswa berhasil di simpan ");
        return map;
    }
    
    @CrossOrigin(origins = "*", methods = RequestMethod.GET, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @GetMapping(value = "/getMahasiswa")
    public List<Mahasiswa> getMahasiswa() {
        return mahasiswaService.getMahasiswa();
    }
    
    @CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @PutMapping(value = "/mahasiswa")
    public Map<String, Object> updateMahasiswa(@RequestBody Mahasiswa mahasiswa) {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		mahasiswaService.update(mahasiswa);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat update data mahasiswa");
	        return map;
		}
        
        map.put("success", Boolean.TRUE);
        map.put("info", "data mahasiswa berhasil di update ");
        return map;
    }

}
