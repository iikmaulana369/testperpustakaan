package com.test.perpustakaan.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.test.perpustakaan.entity.Buku;
import com.test.perpustakaan.entity.Peminjaman;
import com.test.perpustakaan.entity.Pengembalian;
import com.test.perpustakaan.services.BukuService;
import com.test.perpustakaan.services.PeminjamanService;
import com.test.perpustakaan.services.PengembalianService;

@RestController
@RequestMapping(value = "/api")
public class PengembalianContoller {
	
	public Peminjaman peminjaman = new Peminjaman();
	public Buku buku = new Buku();
	
	@Autowired
    private BukuService bukuService;
	
	@Autowired
    private PeminjamanService peminjamanService;

	@Autowired
	private PengembalianService pengembalianService;
	
	@Autowired
	private KafkaTemplate<Object, Object> kafkaTemplate;

	@CrossOrigin(origins = "*", methods = RequestMethod.POST, maxAge = 3600, allowedHeaders = { "accept", "authorization", "x-requested-with", "content-type" })
	@PostMapping(value = "/pengembalian/{idPeminjaman}")
	public Map<String, Object> savePengembalian(@PathVariable (value = "idPeminjaman") String idPeminjaman, @RequestBody Pengembalian pengembalian) {
		
		Map<String, Object> map = new HashMap<String, Object>();
    	
    	try {
    		peminjaman = peminjamanService.getPeminjaman(idPeminjaman);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat mengambil data peminjaman");
	        return map;
		}
    	
    	try {
    		buku = bukuService.getBuku(peminjaman.getBuku().getIdBuku());
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat mengambil data buku");
	        return map;
		}
    	
		if (peminjaman == null) {
    		map.put("success", Boolean.FALSE);
	        map.put("info", "ID Peminjaman tidak terdaftar");
	        return map;
    	}
    	
    	if (buku == null) {
    		map.put("success", Boolean.FALSE);
	        map.put("info", "ID Buku tidak terdaftar");
	        return map;
    	}
    	
    	pengembalian.setPeminjaman(peminjaman);
    	pengembalian.setBuku(buku);
    	
    	try {
    		pengembalianService.save(pengembalian);
    		
    		this.kafkaTemplate.send("pengembalian", pengembalian);
    		
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat menyimpan data pengembalian");
	        return map;
		}

    	buku.setJumlahBuku(buku.getJumlahBuku() + peminjaman.getJumlah());
    	
    	try {
    		bukuService.update(buku);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat update data buku");
	        return map;
		}
    	
    	peminjaman.setStatus(false);
    	
    	try {
    		peminjamanService.update(peminjaman);
		} catch (Exception e) {
	        map.put("success", Boolean.FALSE);
	        map.put("info", "Terjadi kesalahan saat update data peminjaman");
	        return map;
		}
        
        map.put("success", Boolean.TRUE);
		map.put("info", "data pengembalian berhasil di simpan ");
		return map;
	}
	
	@CrossOrigin(origins = "*", methods = RequestMethod.GET, maxAge = 3600, allowedHeaders = {"accept", "authorization", "x-requested-with", "content-type"})
    @GetMapping(value = "/getPengembalian")
    public List<Pengembalian> getPengembalian() {
        return pengembalianService.getPengembalian();
    }

}