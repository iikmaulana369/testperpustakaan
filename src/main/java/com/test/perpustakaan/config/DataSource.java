package com.test.perpustakaan.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Configuration
@PropertySource("classpath:application.yml")
public class DataSource {

	@Autowired
	private Environment environment;

	@Bean(destroyMethod = "close")
	public HikariDataSource dataSourceConfig() {
		HikariConfig hikaryConfig = new HikariConfig();
		hikaryConfig.setDriverClassName(environment.getRequiredProperty("spring.datasource.driver-class-name"));
		hikaryConfig.setJdbcUrl(environment.getRequiredProperty("spring.datasource.url"));
		hikaryConfig.setUsername(environment.getRequiredProperty("spring.datasource.username"));
		hikaryConfig.setPassword(environment.getRequiredProperty("spring.datasource.password"));
		hikaryConfig.setMaximumPoolSize( Integer.parseInt(environment.getRequiredProperty("spring.datasource.maximumPoolSize")));
		hikaryConfig.setMinimumIdle(Integer.parseInt(environment.getRequiredProperty("spring.datasource.minimumIdle")));
		hikaryConfig.setMaxLifetime(Integer.parseInt(environment.getRequiredProperty("spring.datasource.maxLifetime")));
		hikaryConfig.setConnectionTimeout(Long.parseLong(environment.getRequiredProperty("spring.datasource.connectionTimeout")));
		hikaryConfig.setIdleTimeout(Long.parseLong(environment.getRequiredProperty("spring.datasource.idleTimeout")));
		hikaryConfig.addDataSourceProperty("poolName",environment.getRequiredProperty("spring.datasource.poolName"));
		hikaryConfig.addDataSourceProperty("cachePrepStmts", "true");
		hikaryConfig.addDataSourceProperty("prepStmtCacheSize", "250");
		hikaryConfig.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");
		return new HikariDataSource(hikaryConfig);
	}

}
