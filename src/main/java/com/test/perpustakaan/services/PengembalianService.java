package com.test.perpustakaan.services;

import java.util.List;

import com.test.perpustakaan.entity.Pengembalian;

public interface PengembalianService {
	
	public void save(Pengembalian pengembalian);
	
	public List<Pengembalian> getPengembalian();

}
