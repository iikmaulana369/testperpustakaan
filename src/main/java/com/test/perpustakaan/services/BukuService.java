package com.test.perpustakaan.services;

import java.util.List;

import com.test.perpustakaan.entity.Buku;

public interface BukuService {

	public void save(Buku buku);

	public void update(Buku buku);
	
	public List<Buku> getBuku();

	public Buku getBuku(String idBuku);

}
