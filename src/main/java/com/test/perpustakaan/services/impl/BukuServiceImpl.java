package com.test.perpustakaan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.perpustakaan.entity.Buku;
import com.test.perpustakaan.repository.BukuRepository;
import com.test.perpustakaan.services.BukuService;

@Service
@Transactional(readOnly = true)
public class BukuServiceImpl implements BukuService {

	@Autowired
	private BukuRepository bukuRepository;

	@Transactional
    @Override
	public void save(Buku buku) {
		bukuRepository.save(buku);
	}

	@Transactional
    @Override
	public void update(Buku buku) {
		bukuRepository.save(buku);
	}

    @Override
	public List<Buku> getBuku() {
        return (List<Buku>) bukuRepository.findAll();
    }

    @Override
	public Buku getBuku(String idBuku) {
		return bukuRepository.findById(idBuku).get();
	}

}