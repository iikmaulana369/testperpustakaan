package com.test.perpustakaan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.perpustakaan.entity.Mahasiswa;
import com.test.perpustakaan.repository.MahasiswaRepository;
import com.test.perpustakaan.services.MahasiswaService;

@Service
public class MahasiswaServiceImpl implements MahasiswaService {

	@Autowired
	private MahasiswaRepository mahasiswaRepository;

	public void save(Mahasiswa mahasiswa) {
		mahasiswaRepository.save(mahasiswa);
	}
	
	public void update(Mahasiswa mahasiswa) {
		mahasiswaRepository.save(mahasiswa);
	}
	
	public List<Mahasiswa> getMahasiswa() {
		return (List<Mahasiswa>) mahasiswaRepository.findAll();
	}

	public Mahasiswa getMahasiswa(String nim) {
		return mahasiswaRepository.findById(nim).get();
	}

}
