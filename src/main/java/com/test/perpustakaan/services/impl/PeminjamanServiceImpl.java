package com.test.perpustakaan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.test.perpustakaan.entity.Peminjaman;
import com.test.perpustakaan.repository.PeminjamanRepository;
import com.test.perpustakaan.services.PeminjamanService;

@Service
public class PeminjamanServiceImpl implements PeminjamanService {

	@Autowired
	private PeminjamanRepository peminjamanRepository;

	public void save(Peminjaman peminjaman) {
		peminjamanRepository.save(peminjaman);
	}
	
	public void update(Peminjaman peminjaman) {
		peminjamanRepository.save(peminjaman);
	}
	
	public List<Peminjaman> getPeminjaman() {
		return (List<Peminjaman>) peminjamanRepository.findAllPeminjaman();
	}

	public Peminjaman getPeminjaman(String idPeminjaman) {
		return peminjamanRepository.findById(idPeminjaman).get();
	}

}