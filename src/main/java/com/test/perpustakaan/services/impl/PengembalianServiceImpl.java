package com.test.perpustakaan.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.perpustakaan.entity.Pengembalian;
import com.test.perpustakaan.repository.PengembalianRepository;
import com.test.perpustakaan.services.PengembalianService;

@Service
@Transactional(readOnly = true)
public class PengembalianServiceImpl implements PengembalianService{
	
	@Autowired
	private PengembalianRepository pengembalianRepository;

	public void save(Pengembalian pengembalian) {
		pengembalianRepository.save(pengembalian);
	}
	
	public List<Pengembalian> getPengembalian() {
		return (List<Pengembalian>) pengembalianRepository.findAll();
	}

}
