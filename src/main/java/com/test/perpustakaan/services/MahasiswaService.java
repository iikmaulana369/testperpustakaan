package com.test.perpustakaan.services;

import java.util.List;

import com.test.perpustakaan.entity.Mahasiswa;

public interface MahasiswaService {

	public void save(Mahasiswa mahasiswa);
	
	public void update(Mahasiswa mahasiswa);

	public List<Mahasiswa> getMahasiswa();
	
    public Mahasiswa getMahasiswa(String nim);

}
