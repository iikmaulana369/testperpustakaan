package com.test.perpustakaan.services;

import java.util.List;

import com.test.perpustakaan.entity.Peminjaman;

public interface PeminjamanService {
	
	public void save(Peminjaman peminjaman);
	
	public void update(Peminjaman peminjaman);
	
	public List<Peminjaman> getPeminjaman();
	
	public Peminjaman getPeminjaman(String idPeminjaman);

}
