package com.test.perpustakaan.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.ForeignKey;
import javax.persistence.ConstraintMode;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "peminjaman")
@Table(name = "peminjaman")
public class Peminjaman {
	
	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id_peminjaman", length = 36)
    private String idPeminjaman;

    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_peminjaman")
    private Date tanggalPeminjaman;

    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_batas_pengembalian", length = 50)
    private Date tanggalBatasPengembalian;
    
    @Column(name = "jumlah", length = 11)
	private Integer jumlah;
    
    @JsonIgnore
    @Column(name = "status", nullable = false)
	private Boolean status = true;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Buku.class)
    @JoinColumn(name = "id_buku", nullable = true, foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private Buku buku;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Mahasiswa.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "npm", nullable = false, foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private Mahasiswa mahasiswa;

	public String getIdPeminjaman() {
		return idPeminjaman;
	}

	public void setIdPeminjaman(String idPeminjaman) {
		this.idPeminjaman = idPeminjaman;
	}

	public Date getTanggalBatasPengembalian() {
		return tanggalBatasPengembalian;
	}

	public void setTanggalBatasPengembalian(Date tanggalBatasPengembalian) {
		this.tanggalBatasPengembalian = tanggalBatasPengembalian;
	}

	public Integer getJumlah() {
		return jumlah;
	}

	public void setJumlah(Integer jumlah) {
		this.jumlah = jumlah;
	}
	
	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	public Buku getBuku() {
		return buku;
	}

	public void setBuku(Buku buku) {
		this.buku = buku;
	}

	public Mahasiswa getMahasiswa() {
		return mahasiswa;
	}

	public void setMahasiswa(Mahasiswa mahasiswa) {
		this.mahasiswa = mahasiswa;
	}

	public Date getTanggalPeminjaman() {
		return tanggalPeminjaman;
	}

	public void setTanggalPeminjaman(Date tanggalPeminjaman) {
		this.tanggalPeminjaman = tanggalPeminjaman;
	}

}
