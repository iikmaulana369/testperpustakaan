package com.test.perpustakaan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity(name = "mahasiswa")
@Table(name = "mahasiswa")
public class Mahasiswa {
	
	@Id
    @Column(name = "nim", length = 8)
    private String nim;

    @Column(name = "nama", length = 50)
    private String nama;

    @Column(name = "kelas", length = 6)
    private String kelas;

    @Lob
    @Column(name = "alamat", length = 50)
    private String alamat;

	public String getNim() {
		return nim;
	}

	public void setNim(String nim) {
		this.nim = nim;
	}

	public String getNama() {
		return nama;
	}

	public void setNama(String nama) {
		this.nama = nama;
	}

	public String getKelas() {
		return kelas;
	}

	public void setKelas(String kelas) {
		this.kelas = kelas;
	}

	public String getAlamat() {
		return alamat;
	}

	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
    
}
