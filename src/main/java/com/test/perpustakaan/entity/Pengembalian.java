package com.test.perpustakaan.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ConstraintMode;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity(name = "pengembalian")
@Table(name = "pengembalian")
public class Pengembalian {

	@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "id_pengembalian", length = 36)
    private String idPengembalian;

    @Temporal(TemporalType.DATE)
    @Column(name = "tanggal_pengembalian")
    private Date tanggalPengembalian;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Buku.class)
    @JoinColumn(name = "id_buku", nullable = true, foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private Buku buku;
    
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Peminjaman.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "id_peminjaman", nullable = false, foreignKey = @ForeignKey(ConstraintMode.CONSTRAINT))
    private Peminjaman peminjaman;

	public String getIdPengembalian() {
		return idPengembalian;
	}

	public void setIdPengembalian(String idPengembalian) {
		this.idPengembalian = idPengembalian;
	}

	public Date getTanggalPengembalian() {
		return tanggalPengembalian;
	}

	public void setTanggalPengembalian(Date tanggalPengembalian) {
		this.tanggalPengembalian = tanggalPengembalian;
	}

	public Buku getBuku() {
		return buku;
	}

	public void setBuku(Buku buku) {
		this.buku = buku;
	}

	public Peminjaman getPeminjaman() {
		return peminjaman;
	}

	public void setPeminjaman(Peminjaman peminjaman) {
		this.peminjaman = peminjaman;
	}

}
