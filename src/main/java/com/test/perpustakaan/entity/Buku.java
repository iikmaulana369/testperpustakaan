package com.test.perpustakaan.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity(name = "buku")
@Table(name = "buku")
public class Buku{

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(name = "id_buku", length = 36)
	private String idBuku;

	@Column(name = "judul_buku", length = 50)
	private String judulBuku;

	@Column(name = "nama_pengarang", length = 50)
	private String namaPengarang;

	@Column(name = "tahun_terbit", length = 11)
	private Integer tahunTerbit;

	@Column(name = "penerbit", length = 50)
	private String penerbit;

	@Column(name = "jumlah_buku", length = 11)
	private Integer jumlahBuku;

	public String getIdBuku() {
		return idBuku;
	}

	public void setIdBuku(String idBuku) {
		this.idBuku = idBuku;
	}

	public String getJudulBuku() {
		return judulBuku;
	}

	public void setJudulBuku(String judulBuku) {
		this.judulBuku = judulBuku;
	}

	public String getNamaPengarang() {
		return namaPengarang;
	}

	public void setNamaPengarang(String namaPengarang) {
		this.namaPengarang = namaPengarang;
	}

	public Integer getTahunTerbit() {
		return tahunTerbit;
	}

	public void setTahunTerbit(Integer tahunTerbit) {
		this.tahunTerbit = tahunTerbit;
	}

	public String getPenerbit() {
		return penerbit;
	}

	public void setPenerbit(String penerbit) {
		this.penerbit = penerbit;
	}

	public Integer getJumlahBuku() {
		return jumlahBuku;
	}

	public void setJumlahBuku(Integer jumlahBuku) {
		this.jumlahBuku = jumlahBuku;
	}

}
