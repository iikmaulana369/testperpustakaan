package com.test.perpustakaan;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.KafkaListener;

@SpringBootApplication
public class Main implements CommandLineRunner {

    private static final CountDownLatch latch = new CountDownLatch(3);

    @Override
    public void run(String... args) throws Exception {
        latch.await(2, TimeUnit.SECONDS);
    }

    @KafkaListener(id = "pengembalian", topics = "pengembalian")
    public static void listen(String data) throws Exception {
        System.out.println(data);
        latch.countDown();
    }

	public static void main(String[] args) {
		if (args.length > 0) {
			try {
				Main main = new Main();
				main.run(args);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			SpringApplication.run(Main.class, args);
		}
	}
}