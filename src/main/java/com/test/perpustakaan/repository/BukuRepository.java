package com.test.perpustakaan.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.perpustakaan.entity.Buku;

public interface BukuRepository extends CrudRepository<Buku, String> {

}
