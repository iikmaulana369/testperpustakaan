package com.test.perpustakaan.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.perpustakaan.entity.Pengembalian;

public interface PengembalianRepository extends CrudRepository<Pengembalian, String> {

}
