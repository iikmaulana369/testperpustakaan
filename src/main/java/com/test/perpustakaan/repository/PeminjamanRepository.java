package com.test.perpustakaan.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.test.perpustakaan.entity.Peminjaman;

public interface PeminjamanRepository extends CrudRepository<Peminjaman, String> {
	
	@Query("SELECT p FROM peminjaman p WHERE p.status = 1") 
	public List<Peminjaman> findAllPeminjaman();

}
