package com.test.perpustakaan.repository;

import org.springframework.data.repository.CrudRepository;

import com.test.perpustakaan.entity.Mahasiswa;

public interface MahasiswaRepository extends CrudRepository<Mahasiswa, String> {

}
